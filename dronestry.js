const fs = require('fs');

let parsedData = {}; 

// Function to read and parse JSON data from a file
function readJsonData(filename) {
    const jsonData = fs.readFileSync(filename, 'utf-8');
    return JSON.parse(jsonData);
}

// Function to update parsed data with new orders
function updateDataWithNewOrders(newOrders) {
    // Append new orders to the existing orders array
    parsedData.orders.push(...newOrders);
}

// Finding the time that's needed to complete the order
function findTime(warehouseX, warehouseY, customerX, customerY) {
    const x = Math.abs(warehouseX - customerX);
    const y = Math.abs(warehouseY - customerY);

    const diagonalMoves = Math.min(x, y);
    const straightMoves = Math.abs(x - y);
    const totalMoves = diagonalMoves + straightMoves;

    const distance = Math.sqrt(Math.pow((customerX - warehouseX), 2) + Math.pow((customerY - warehouseY), 2));
    if (totalMoves>distance)
    {
        totalMoves=distance;
    }
    return totalMoves + 5; // 1min for every move + 5 min picking up the order
}

// Function to find customer coordinates by ID
function findCustomer(customerID) {
    for(let i = 0; i < parsedData.customers.length; i++)
    {
        if (parsedData.customers[i].id===customerID)
        {
    
            return parsedData.customers[i].coordinates;
        }
    }
    return null;
}

// Function to extract numbers from strings using regular expressions
function extractNumber(str) {
    const kW = /(\d+)kW/;
    const W = /(\d+)W/;
    let match = str.match(kW);
    if (match) {
        return parseInt(match[1])*1000; // converting kW to W
    }
    else if (!match)
    {
        match = str.match(W);
        return parseInt(match[1]);
    }
    return 0; // Return 0 if no match is found
}

// Function to generate autoincremented ids for drones
let droneIdCounter = 0;
function generateId() {
    return ++droneIdCounter;
}

function useDroneForOrder(drone, distance) {
    drone.capacity -= (distance * drone.consumption) * 2;
    drone.capacity+=5; //adding 5 minutes because the drone picks the order only once
    if (drone.capacity <= 0) {
        drone.available = false;
    }
    return {
        capacity: drone.capacity,
        available: drone.available
    };
}

function findDrone(drones, minDistance) {
    let tempDrone = null;
    let minCapacity = Infinity;

    for (let k = 0; k < drones.length; k++) {
        let drone = drones[k];
        const batteryUsage = minDistance * drone.consumption;

        if (drone.available && drone.capacity >= batteryUsage && drone.capacity < minCapacity) {
            minCapacity = drone.capacity;
            tempDrone = drone;
        }
    }

    return tempDrone;
}

function makeOrder(tempDrone, minDistance, isPoweredOn, usedDrones, orderID, totalDeliveryTime) {
    if (tempDrone) {
        let updatedDrone = useDroneForOrder(tempDrone, minDistance); // Removing the second 5 minutes 
        totalDeliveryTime += minDistance;
        if (isPoweredOn) {
            console.log('Minimum distance for order', orderID + ':', minDistance);
            console.log('Chosen drone for order', orderID + ':', tempDrone.id);
            console.log('Updated drone capacity:', updatedDrone.capacity);
            console.log('Drone availability:', updatedDrone.available);
        }
        usedDrones.add(tempDrone.id);
    } else {
        // If no available drone can handle the order, charge the drone with the lowest capacity
        let lowestCapacityDrone = drones[0]; // Initialize with the first drone
        for (let i = 1; i < drones.length; i++) {
            if (drones[i].capacity < lowestCapacityDrone.capacity) {
                lowestCapacityDrone = drones[i]; // Update if the current drone has lower capacity
            }
        }

        if (isPoweredOn) {
            console.log('No available drone found. Charging drone with id:', lowestCapacityDrone.id);
        }

        totalDeliveryTime += 20;
        lowestCapacityDrone.capacity += 20 * lowestCapacityDrone.consumption;
    }
    return totalDeliveryTime;
}

function runprogram(programMilliseconds, isPoweredOn) {

    let totalDeliveryTime = 0;
    let drones = parsedData.typesOfDrones.map(drone => {
        return {
            id: generateId(), // Autoincremented id
            capacity: extractNumber(drone.capacity),
            consumption: extractNumber(drone.consumption),
            available: true, // Initially all drones are available
        };
    });

    let usedDrones = new Set();

    let ordersLength = parsedData.orders.length;

    for (let i = 0; i < ordersLength; i++) {
        let customerCoordinates = findCustomer(parsedData.orders[i].customerId);
        let minDistance = Infinity;

        for (let j = 0; j < parsedData.warehouses.length; j++) {
            const warehouse = parsedData.warehouses[j];
            const warehouseX = warehouse.x;
            const warehouseY = warehouse.y;
            const distance = findTime(warehouseX, warehouseY, customerCoordinates.x, customerCoordinates.y);
            minDistance = Math.min(minDistance, distance);
        }

        let tempDrone = findDrone(drones, minDistance);

        totalDeliveryTime = makeOrder(tempDrone, minDistance, isPoweredOn, usedDrones, parsedData.orders[i].id, totalDeliveryTime);
    }
    let averageDeliveryTime = totalDeliveryTime / ordersLength;

    console.log("Max Number of drones needed:", ordersLength);
    console.log("Total delivery time for all orders:", totalDeliveryTime, "minutes");
    console.log("Average delivery time per order:", averageDeliveryTime, "minutes");
    console.log("Number of drones used:", usedDrones.size);
    
    if (parsedData.output.minutes.program > 0) {
        setTimeout(() => {
            process.exit(0);
        }, programMilliseconds);
    }
}

function start() {
    const programMinutes = parsedData.output.minutes.program;
    const realMilliseconds = parsedData.output.minutes.real;
    const programMilliseconds = (programMinutes / realMilliseconds) * 60000; // Convert minutes to milliseconds

    runprogram(programMilliseconds, parsedData.output.poweredOn);
  
    // Read the configuration file periodically (every program minute)
    setInterval(() => {
      readConfigFile();
      // Process new orders here
    }, programMinutes * 60000);
}

// Read initial data from JSON file
parsedData = readJsonData('db.json');

start();
